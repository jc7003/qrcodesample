package com.example.owen.qrcordsample;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.owen.fragment.EncodeFragment;
import com.example.owen.fragment.ScannerFragment;

public class MainActivity extends AppCompatActivity {

    private Fragment mCurrentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(mCurrentFragment instanceof ScannerFragment) {
            (mCurrentFragment).onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case R.id.action_qrcode:
                if (mCurrentFragment == null || !(mCurrentFragment instanceof ScannerFragment)) {
                    Fragment f  = ScannerFragment.newInstance();
                    changeFragment(f);
                    mCurrentFragment = f;
                } else{
                    ((ScannerFragment) mCurrentFragment).showCodeScanner();
                }
                break;
            case R.id.action_scanner:
                if (mCurrentFragment == null || !(mCurrentFragment instanceof EncodeFragment)) {
                    Fragment f  = EncodeFragment.newInstance();
                    changeFragment(f);
                    mCurrentFragment = f;
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void changeFragment(Fragment f) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(mCurrentFragment !=null)
            transaction.remove(mCurrentFragment);

        transaction.replace(R.id.main_fragment_layout, f);

        transaction.commitAllowingStateLoss();
        mCurrentFragment = f;
    }
}
