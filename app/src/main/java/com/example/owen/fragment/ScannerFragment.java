package com.example.owen.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.owen.qrcordsample.R;

import java.util.List;
import java.util.Locale;

/**
 * Created by owen on 2016/9/17.
 */
public class ScannerFragment extends Fragment{

    private static final String[] STRING_INSTALL_TITLE = new String[] { "Install Barcode Scanner", "安裝條碼掃描器" };
    private static final String[] STRING_INSTALL_CLOSE = new String[] { "Close", "關閉" };
    private static final String[] STRING_INSTALL_INSTALL = new String[] { "Install", "安裝" };
    private static final String[] STRING_INSTALL_MESSAGE = new String[] { "In order to scan barcode, you neet to install Zxing Barcode Scanner first.", "為了能夠掃描條碼，您需要一個條碼掃描器。" };
    private static final String[] STRING_URL_QRCODE_SCANNER = new String[] { "https://play.google.com/store/apps/details?id=com.google.zxing.client.android" };

    private final String SCANNER_PACKAGE="com.google.zxing.client.android";
    private final String SCANNER = SCANNER_PACKAGE + ".SCAN";
    private TextView mTextView;

    private final int QRCODE_SCANNER = 0X1001;

    public static ScannerFragment newInstance() {
        ScannerFragment f = new ScannerFragment();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        View root = inflater.inflate(R.layout.fragment_scanner, container, false);
        mTextView = (TextView)root.findViewById(R.id.textView);

        showCodeScanner();
        return root;
    }

    public void showCodeScanner(){
        if(isAdded() || isVisible()) {
            Intent intent = new Intent(SCANNER);

            if (this.getContext().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY).size() == 0) {
                //未安裝
                showDownloadTip();
                return;
            }
            // SCAN_MODE, 可判別所有支援的條碼
            // QR_CODE_MODE, 只判別 QRCode
            // PRODUCT_MODE, UPC and EAN 碼
            // ONE_D_MODE, 1 維條碼
            intent.putExtra("SCAN_MODE", "SCAN_MODE");

            // 呼叫ZXing Scanner，完成動作後回傳 1 給 onActivityResult 的 requestCode 參數
            startActivityForResult(intent, QRCODE_SCANNER);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        if(requestCode == QRCODE_SCANNER)
        {
            if(resultCode == Activity.RESULT_OK) {
                // ZXing回傳的內容
                String contents = intent.getStringExtra("SCAN_RESULT");
                contents += "\n";
                mTextView.setText(contents.toString());
            }
            else if(resultCode == Activity.RESULT_CANCELED){
                Toast.makeText(getContext(), "取消掃描", Toast.LENGTH_SHORT);
            }
        }
    }

    private void showDownloadTip(){
        new AlertDialog.Builder(getActivity()).setTitle(getString(STRING_INSTALL_TITLE)).setMessage(getString(STRING_INSTALL_MESSAGE)).setPositiveButton(getString(STRING_INSTALL_INSTALL), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                final String url = getString(STRING_URL_QRCODE_SCANNER);
                final Intent openURLIntent = new Intent(Intent.ACTION_VIEW);
                openURLIntent.setData(Uri.parse(url));
                final List<ResolveInfo> list = getActivity().getPackageManager().queryIntentActivities(openURLIntent, PackageManager.MATCH_DEFAULT_ONLY);
                if (list.size() > 0) {
                    final Intent destIntent = Intent.createChooser(openURLIntent, null);
                    getActivity().startActivity(destIntent);
                }
            }

        }).setNegativeButton(getString(STRING_INSTALL_CLOSE), null).show();
    }

    // -----類別變數-----
    private static boolean chinese = isChinese();

    // -----類別方法-----
    private static boolean isChinese() {
        final Locale locale = Locale.getDefault();
        return locale.equals(Locale.CHINESE) || locale.equals(Locale.SIMPLIFIED_CHINESE) || locale.equals(Locale.TRADITIONAL_CHINESE);
    }

    private static String getString(final String[] string) {
        int index = chinese ? 1 : 0;
        if (index >= string.length) {
            index = 0;
        }
        return string[index];
    }
}
