package com.example.owen.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.owen.qrcordsample.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.HashMap;

/**
 * Created by owen on 2016/9/17.
 */
public class EncodeFragment extends Fragment{

    private EditText mEditText;
    private Button mButton;
    private ImageView mImageView;

    public static EncodeFragment newInstance() {
        EncodeFragment f = new EncodeFragment();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        View root = inflater.inflate(R.layout.fargment_encode, container, false);
        mEditText = (EditText)root.findViewById(R.id.editText);
        mButton = (Button)root.findViewById(R.id.button);
        mButton.setOnClickListener(mEncodeListener);

        mImageView = (ImageView)root.findViewById(R.id.qrcode);

        return root;
    }

    private View.OnClickListener mEncodeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String content = mEditText.getText().toString();
            if(content !=null && content.length() > 0){
                try {
                    mImageView.setImageBitmap(encodeAsBitmap(content, BarcodeFormat.QR_CODE, 450, 450));
                } catch (WriterException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    public static Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int desiredWidth, int desiredHeight) throws WriterException
    {
        if (contents.length() == 0) return null;
        final int WHITE = 0xFFFFFFFF;
        final int BLACK = 0xFF000000;
        HashMap<EncodeHintType, String> hints = null;
        String encoding = null;
        for (int i = 0; i < contents.length(); i++)
        {
            if (contents.charAt(i) > 0xFF)
            {
                encoding = "UTF-8";
                break;
            }
        }
        if (encoding != null)
        {
            hints = new HashMap<>(2);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result = writer.encode(contents, format, desiredWidth, desiredHeight, hints);
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++)
        {
            int offset = y * width;
            for (int x = 0; x < width; x++)
            {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }
}
